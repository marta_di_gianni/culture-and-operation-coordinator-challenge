# Culture and Operation Coordinator Challenge

### **Il Problema**

Dobbiamo organizzare la cena/pranzo di Natale e **tu hai il compito di organizzare tutto.** 
È un problema apparentemente semplice, ma che nasconde delle insidie.
Coordinare tante persone è sempre complicato. Ci sono tanti fattori da tenere in considerazione e la scelta della data (ad esempio) può essere fatta secondo tanti criteri differenti.

Ma non ti preoccupare. Natale è passato e abbiamo organizzato un pranzo meraviglioso (anche se le foto sono orribili).

Quindi iniziamo!

## Indice
- [Cosa devi fare](#cosa-devi-fare)
- [Struttura del Team](#struttura-del-team)
- [Scenario](#scenario)
- [Come inviare la soluzione](#come-fare-per-inviare)
- [Perchè](#perche)
- [Domande](#domande)

## Cosa devi fare

Questo è un esercizio di decomposizione di un problema.

Quindi **dovrai descrivere tutti gli step e i passi che effettueresti per arrivare al giorno del pranzo / cena**.

Dovrai essere **Mini CEO** di questa attività, non dare nulla per scontato e non lasciare nulla al caso.

Molti degli step da compiere dipenderanno da risposte che dovrai ricevere da alcuni membri del team. Siccome non è possibile simulare questi processi, puoi scegliere se definire uno o più scenari ipotizzando le risposte che potresti ricevere.

Ricordati che in BitBoss applichiamo il principio del **No Lazy Thinking** di cui abbiamo parlato brevemente [qui](https://www.bitboss.it/inside-bitboss/welcome-to-bitboss-backlog).

### Struttura del Team

BitBoss è una Software House con sede a Torino.
Ci sono 5 co-founder che hanno ancora ruoli operativi.
Ha un team che lavora con frequenza regolare nel nostro ufficio di Torino e che consideriamo il nostro Team Interno.
All’interno di questo team interno ci sono anche persone con contratti di collaborazione in partita iva, ma dal punto di vista del rapporto umano non c’è alcuna differenza rispetto ai dipendenti.

BitBoss inoltre collabora con altri professionisti in partita iva che operano in full remote.

Alcuni di questi risiedono non troppo distante da Torino e saltuariamente vengono a trovarci.
Alcuni abitano distante, ma con frequenza minore passano a Torino a trovarci.
Altri non li abbiamo mai conosciuti di persona.

Se fossero tutti a Torino, sarebbero tutti invitati.

### Scenario ipotetico

È il 15 di Dicembre e il tempo stringe. Siamo leggermente in ritardo quindi alcune persone potrebbero avere già programmi per i giorni a ridosso del Natale.

## Come fare per inviare la tua soluzione quando hai finito

Per fare questo test abbiamo scelto ***GitLab*** è una piattaforma web di gestione di repository Git. Si tratta di uno strumento che utilizziamo nei processi sviluppo dei prodotti digitali che creiamo.

Sicuramente ti troverai in un ambiente che non ti è familiare. Anche questo è parte del test.

Per inviare la tua soluzione **apri una nuova ISSUE qui su GitLab (ricordati di flaggarla come privata), scrivi, allega o linka la tua soluzione e inviala.**
Può essere un semplice testo, un video, una presentazione o quello che vuoi tu.

## Perché ti sottoponiamo questo test?

Non abbiamo alcun interessa a capire come risolveresti questo task specifico, ma ci interessa capire come ragioni e come organizzi le informazioni per trasferirle ad altre persone.

Non esiste un modo giusto e un modo sbagliato di procedere. 
È una simulazione per definizione imperfetta, che non ti permette di apprendere dal contesto e modificare di conseguenza il tuo modo di agire. Ne siamo consapevoli.

Ovviamente questo è un compito che nella realtà può essere tranquillamente eseguito ad intuito, affidandosi a buon senso e ragionevolezza. Tuttavia ti chiediamo di over ingegnerizzare un processo apparentemente semplice, perché crediamo in questo modo di ottenere delle informazioni utili e interessanti sul tuo modo di ragionare.

### Domande

Se ritieni che le informazioni che abbiamo dato non siano sufficienti o non hai compreso alcune cose, invia le tue domande all'indirizzo hey@jobs.bitboss.it  

**In bocca al lupo!**
